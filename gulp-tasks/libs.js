module.exports = function (gulp, plugins) {
	return function () {
		gulp.src([
			'node_modules/jquery/dist/jquery.min.js',
			'node_modules/slick-carousel/slick/slick.min.js',
			'node_modules/inputmask/dist/jquery.inputmask.bundle.js',
			'node_modules/jquery-gray/js/jquery.gray.min.js',
			// 'node_modules/breakpoints-js/dist/breakpoints.min.js',
			])
		.pipe(plugins.concat('libs.js'))
		.pipe(gulp.dest('_src/libs'));
	};
};