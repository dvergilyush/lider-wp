<?php 

	$terms = get_terms( array(
	'taxonomy'      => array( 'subservices' ), // название таксономии с WP 4.5
	'orderby'       => 'id', 
	'order'         => 'ASC',
	'hide_empty'    => false,
	) );

	if( $terms && ! is_wp_error($terms) ){ ?>
		<div class='row'>
			<?php foreach( $terms as $term ){
					$image = get_field('background_image', $term);
				?>

				<div class="col-12 col-md-6 page__content-col">
					<div class="tile tile_size_big" style="background-image: url(<?php echo $image ?>)">
						<div class="tile__wrap overlay-bg relative">
							<div class="tile__title">
								<div class="tile__title-img-wrap tile__title-img-wrap_type_bg">
									<img src="<?php echo get_template_directory_uri();?>/img/index/service/icon-1.png" alt="" class="tile__img">
								</div>
								<div class="tile__title-text uppercase"><?php echo $term->name ?></div>
							</div>
						</div>
					</div>
				</div>

			<?php } ?>
		</div>
<?php } ?>