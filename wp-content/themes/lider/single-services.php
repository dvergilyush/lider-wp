<?php get_header(); ?>

<section class="page__category">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-7 col-lg-8 page__content-wrap">
				<?php if ( have_posts() ) :  while ( have_posts() ) : the_post(); ?>
					<h2 class="page__title"><?php the_title(); ?></h2>
					<div class="row page__subtitle">
						<?php
							if ( function_exists('yoast_breadcrumb') ) {
								yoast_breadcrumb('<p id="breadcrumbs">','</p>');
							}
						?>
					</div>
					<div class="page__content">
						<div class="page__text">

							<?php if ( has_post_thumbnail() ) { ?>
							<div class="page__img-wrap">
								<img class="page__img" src="<?php the_post_thumbnail_url('large'); ?>" alt="">
							</div>
							<?php } ?>
						
						
							<?php the_content(); ?>
						</div>
					</div>
				<?php endwhile; ?>
				<?php endif; ?>
			</div>

			<div class="col-12 col-md-5 col-lg-4 page__aside-wrap">
				<aside class="page__aside aside">

					<?php 

						$terms = get_terms( array(
						'taxonomy'      => array( 'subservices' ), // название таксономии с WP 4.5
						'orderby'       => 'id', 
						'order'         => 'ASC',
						'exclude'          => 16,
						'hide_empty'    => false,
						) );

						if( $terms && ! is_wp_error($terms) ){ ?>

							<?php foreach( $terms as $term ){
									$image = get_field('background_image', 'subservices_' .$term->term_id);
									$icon = get_field('title_img', 'subservices_' .$term->term_id);
									$descr = $term->description;
									$description = mb_substr($descr, 0, 300) . "...";

							?>

								<div class="aside__item">
									<div class="tile tile_size_sm">
										<div class="tile__wrap tile__wrap_type_sm overlay-bg relative" style="background-image: url(<?php echo $image; ?>); background-size: cover; background-position: center;">
											<div class="tile__title-img-wrap tile__title-img-wrap_sm ">
												<img src="<?php echo $icon; ?>" alt="" class="tile__img">
											</div>
											<div class="tile__title-text tile__title-text_type_sm uppercase"><?php echo $term->name ?></div>
											<a href="<?php echo get_term_link($term->term_id); ?>" class="btn-link tile__btn">
												<span class="btn-link__text uppercase">Подробнее</span>
												<span class="btn-link__icon"><?php include "parts/commons/icons/right-arrow.php"?></span>
											</a>
										</div>
									</div>
								</div>

								<?php }
							} ?>


				</aside>
			</div>
		</div>
	</div>
</section>
<?php get_post(); ?>
<?php get_footer(); ?>