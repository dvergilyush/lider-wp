<?php get_header(); ?>
<section class="page__category">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-7 col-lg-8 page__content-wrap">
				<?php if ( have_posts() ) :  while ( have_posts() ) : the_post(); ?>
					<h2 class="page__title"><?php the_title(); ?></h2>
					<div class="row page__subtitle">
						<?php
							if ( function_exists('yoast_breadcrumb') ) {
								yoast_breadcrumb('<p id="breadcrumbs">','</p>');
							}
						?>
					</div>
					<div class="page__content">

						<?php if ( has_post_thumbnail() ) { ?>
							<div class="page__img-wrap">
								<img class="page__img" src="<?php the_post_thumbnail_url('large'); ?>" alt="">
							</div>
						<?php } ?>

						<?php the_content(); ?>
					</div>
				<?php endwhile; ?>
				<?php endif; ?>
			</div>

			<div class="col-12 col-md-5 col-lg-4 page__aside-wrap">
					<aside class="page__aside">
						<?php
							// параметры по умолчанию
							$posts = get_posts( array(
								'numberposts' => 2,
								'category' => get_the_terms($post->ID, array_keys(get_the_taxonomies($post))[0])[0]->slug,
								'orderby'     => 'date',
								'order'       => 'DESC',
								'exclude'     => array($post->ID),
								'post_type'   => $post->post_type,
								'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
							) );
							foreach( $posts as $post ){
								setup_postdata($post); ?>
									<div class="aside__item">
										<div class="card">
											<div class="card__img-wrap">
												<?php if ( has_post_thumbnail() ) { ?>
												<img class="card__img" src="<?php the_post_thumbnail_url('medium'); ?>" alt="">
												<?php } else { ?>
												<img class="card__img card__img_default" src="<?php bloginfo('template_directory'); ?>/img/images-default.png" alt="<?php the_title(); ?>" />
												<?php } ?>
											</div>
											<div class="card__body">
												<div class="card__date"><?php the_time('j F Y в H:i'); ?></div>
												<div class="card__title"><?php the_title(); ?></div>
												<div class="card__text"><?php the_excerpt(); ?></div>
												<a class="card__btn button" href="<?php the_permalink() ?>">Подробнее</a>
											</div>
										</div>
									</div>
							<?php }

							wp_reset_postdata(); // сброс
						?>

							<a href="<?php home_url(); ?>/news-rubric/all-news/" class="btn-link btn-link_mobile">
									<span class="btn-link__text uppercase">Все новости</span>
									<span class="btn-link__icon"><?php include "parts/commons/icons/right-arrow.php"?></span>
							</a>

					</aside>
			</div>

		</div>
	</div>
</section>

<?php get_post(); ?>
<?php get_footer(); ?>