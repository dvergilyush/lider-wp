<?php get_header(); ?>
<section class="page__category">

    <div class="container">
        <h2 class="page__title">Error 404</h2>
        <div class="row page__subtitle">
            <?php
                if ( function_exists('yoast_breadcrumb') ) {
                    yoast_breadcrumb('<p id="breadcrumbs">','</p>');
                }
            ?>
        </div>

        <div class="page__content">
            <div class="page__text">
                <?php the_content(); ?>
            </div>
        </div>
    </div>
</section>

<?php get_footer();?>
