<?php get_header();?>

<?php the_post();?>
<section class="contacts page__category">

    <div class="container">
        <h2 class="page__title"><?php echo wp_get_document_title(); ?></h2>
        <div class="row page__subtitle">
            <?php
                if ( function_exists('yoast_breadcrumb') ) {
                    yoast_breadcrumb('<p id="breadcrumbs">','</p>');
                }
            ?>
        </div>

        <div class="page__content">

            <div class="contacts-block">
                <div class="row">

                    <div class="col-12 col-md-4 contacts-block__item">
                        <div class="contacts-block__icon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/contacts/iphone.png" alt=""></div>
                        <div class="contacts-block__label">телефон:</div>
                        <div class="contacts-block__data"><?php the_field('contacts-block_phone');?></div>
                        <a href="tel:<?php the_field('contacts-block_phone');?>" class="btn-link contacts-block__link">
                            <span class="btn-link__text uppercase">Подробнее</span>
                            <span class="btn-link__icon"><?php include "parts/commons/icons/right-arrow.php"?></span>
                        </a>
                    </div>
                    <div class="col-12 col-md-4 contacts-block__item">
                        <div class="contacts-block__icon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/contacts/email.png" alt=""></div>
                        <div class="contacts-block__label">e-mail:</div>
                        <div class="contacts-block__data"><?php the_field('contacts-block_email');?></div>
                        <a href="mailto:<?php the_field('contacts-block_email');?>" class="btn-link contacts-block__link">
                            <span class="btn-link__text uppercase">Написать письмо</span>
                            <span class="btn-link__icon"><?php include "parts/commons/icons/right-arrow.php"?></span>
                        </a>
                    </div>
                    <div class="col-12 col-md-4 contacts-block__item">
                        <div class="contacts-block__icon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/contacts/location.png" alt=""></div>
                        <div class="contacts-block__label">адрес:</div>
                        <div class="contacts-block__data"><?php the_field('contacts-block_address');?></div>
                        <a href="#" class="btn-link contacts-block__link">
                            <span class="btn-link__text uppercase">Показать на карте</span>
                            <span class="btn-link__icon"><?php include "parts/commons/icons/right-arrow.php"?></span>
                        </a>
                    </div>

                </div>
            </div>

            <div class="contacts-form">
                <div class="form">
                    <div class="form__title uppercase">Напишите нам</div>
                    <div class="form__subtitle"><?php the_content();?></div>

                    <?php echo do_shortcode('[contact-form-7 id="90" title="Contact form 1"]')?>
                    <?php // echo do_shortcode('[contact-form-7 id="92" title="qweqw"]')?>
                </div>
            </div>

        </div>
    </div>
</section>

<?php get_footer();?>
