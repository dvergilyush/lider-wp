<footer class="footer site-footer">
	<div class="container">
		<div class="footer__wrap">

			<div class="footer__block footer__policy">
				<div class="footer__text footer__policy-par">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean
					commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis disvitae, justo. Nullam dictum
					felis eu pede mollis pretium. Integ
				</div>
				<div class="footer__text footer__link underline">
					<a href="#">Политика конфиденциальности</a>
				</div>
			</div>
			
			<div class="footer__block footer__sections">
				
				<div class="footer__section">
					<div class="footer__sections-title uppercase">Услуги</div>
					<ul class="footer__sections-list">
						<li class="footer__text"><a href="#">Консалтинг в КНР</a></li>
						<li class="footer__text"><a href="#">Регистрация и открытие компании в КНР</a></li>
						<li class="footer__text"><a href="../marketing.php">Реклама и маркетинг в КНР</a></li>
						<li class="footer__text"><a href="#">Выставки и форумы</a></li>
					</ul>
				</div>

				<div class="footer__section">
					<div class="footer__sections-title uppercase">Бизнес с Китаем</div>
					<ul class="footer__sections-list">
						<li class="footer__text"><a href="#">Выставки и форумы</a></li>
						<li class="footer__text"><a href="#">Выставки</a></li>
						<li class="footer__text"><a href="#">Форумы</a></li>
					</ul>
				</div>
			</div>
			<div class="footer__block footer__contacts">

				<div class="footer__contact-wrap">
					<div class="footer__contact">
						<div class="footer__contacts-label footer__text">телефон:</div>
						<div class="footer__contacts-data">8 (800) 456-78-90</div>
						<div class="footer__contacts-link footer__text underline">
							<a href="tel:+78007894561">позвонить</a>
						</div>
					</div>
					<div class="footer__contact">
						<div class="footer__contacts-label footer__text">e-mail:</div>
						<div class="footer__contacts-data">pochta123@gmail.com</div>
						<div class="footer__contacts-link footer__text underline">
							<a href="mailto:pochta@gmail.com">написать письмо</a></div>
					</div>
					<div class="footer__contact">
						<div class="footer__contacts-label footer__text">адрес:</div>
						<div class="footer__contacts-data">Санкт-Петербург, ул. Ленина, 20-45</div>
						<div class="footer__contacts-link footer__text underline">
							<a href="#">показать на карте</a></div>
					</div>
				</div>


				<div class="footer__socials">
					<a href="#">
						<div class="footer__socials-item social-icon">
							<?php include get_theme_file_path("parts/commons/icons/vk.php")?>
						</div>
					</a>
					<a href="#">
						<div class="footer__socials-item social-icon">
							<?php include get_theme_file_path("parts/commons/icons/facebook.php")?>
						</div>
					</a>
					<a href="#">
						<div class="footer__socials-item social-icon">
							<?php include get_theme_file_path("parts/commons/icons/instagram.php")?>
						</div>
					</a>
					<a href="#">
						<div class="footer__socials-item social-icon">
							<?php include get_theme_file_path("parts/commons/icons/youtube.php")?>
						</div>
					</a>
					<a href="#">
						<div class="footer__socials-item social-icon">
							<?php include get_theme_file_path("parts/commons/icons/wechat.php")?>
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</footer>
<?php include get_theme_file_path('parts/commons/modals.php'); ?>
