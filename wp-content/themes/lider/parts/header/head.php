<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset') ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">
    <meta name="format-detection" content="telephone=no">
    <!-- <link rel="icon" href="favicon.ico"> -->
	<?php wp_head(); ?>

    <style>
        div.wpcf7-validation-errors,
        div.wpcf7-acceptance-missing,
        div.wpcf7-mail-sent-ok,
        div.wpcf7-spam-blocked {
            border: none;
            padding: 0;
            margin: 0;
        }
        span.wpcf7-not-valid-tip {
            font-size: 0.7em;
            padding: 5px 0 0 5px;
            color: rgba(210, 0, 2, 0.8);
        }
        .form__attach {
            cursor: pointer;
        }
        .form__attach-text {
            height: 19px;
            line-height: 19px;
        }
        input[type="file"] {
            display: none;
        }
    </style>
</head>