<section class="main" id="main">

	<div class="main-slider">
		<?php 

			$terms = get_terms( array(
			'taxonomy'      => array( 'subservices' ),
			'orderby'       => 'id', 
			'order'         => 'ASC',
			'exclude'          => 16,
			'hide_empty'    => false,
			) );

			if( $terms && ! is_wp_error($terms) ){ ?>
					<?php foreach( $terms as $term ){
						$descr = $term->description;
						$description = the_excerpt_max_charlength( $descr, 300);
						$slide_bg = get_field('slide_background', 'subservices_' .$term->term_id);
					?>

						<div class="main-slider__item" style="background-image: url(<?php echo $slide_bg; ?>); background-size: cover; background-position: center;">
							<div class="container main-slider__item-container">
								<div class="main-slider__content">
									<h2 class="main-slider__title uppercase"><?php echo $term->name ?></h2>
										<div class="main-slider__descr">
											<?= $description ?>
										</div>
									<a class="main-slider__button button" href="<?php echo get_term_link($term->term_id); ?>">Подробнее</a>
								</div>
							</div>
						</div>

					<?php } ?>
		<?php } ?>

	</div>

</section>

<div class="main-news">
	<div class="main-news__slider">
		<?php
			$posts = get_posts( array(
				'numberposts' => 10,
				'orderby'     => 'date',
				'order'       => 'DESC',
				'post_type'   => 'news',
				'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
			) );
			foreach( $posts as $post ){
				setup_postdata($post); ?>
				<div class="main-news__item overlay-bg relative">
					<div class="main-news__img"><img class="grayscale" src="<?php the_post_thumbnail_url('medium_large'); ?>" alt=""></div>
					<a class="main-news__content" href="<?php the_permalink(); ?>">
						<div class="main-news__date"><?php the_time('j F Y в H:i'); ?></div>
						<div class="main-news__title uppercase">
							<?php 
							$title = get_the_title();
							$titleMin = the_excerpt_max_charlength( $title, 70);
							echo $titleMin;
							?>
								
							</div>
						<div class="main-news__descr"><?php the_excerpt(); ?></div>
						<div class="main-news__link btn-link">
							<span class="btn-link__text uppercase">Подробнее</span>
							<span class="btn-link__icon"><?php include get_theme_file_path("parts/commons/icons/right-arrow.php")?></span>
						</div>
					</a>
				</div>
			<?php }
			wp_reset_postdata(); // сброс
		?>

	</div>

</div>