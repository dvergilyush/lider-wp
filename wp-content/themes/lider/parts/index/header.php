<!DOCTYPE html>
<html lang="ru">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">
	<meta name="format-detection" content="telephone=no">
	<!-- <link rel="icon" href="favicon.ico"> -->
	<title>Международный центр "Лидер"</title>
	<!-- <link rel="stylesheet" href="css/common.min.css"> -->
	<?php wp_head(); ?>
</head>

<body>

<div class="header header_index">

	<div class="header__line header__line_index">

		<div class="container relative">

			<div class="header__burger jsShowMenu">
				<div class="burger burger_dark">
					<span></span>
				</div>
			</div>

			<div class="row flex-nowrap justify-content-between">

				<nav class="header__menu header__menu_index menu nav-menu jsMenu">
					<ul class="menu__list flex-nowrap justify-content-between">
						<li class="menu__item menu__item_main menu-item"><a href="../about.php">О центре</a></li>
						<li class="menu__item menu__item_main menu-item"><a href="../news.php">Новости</a></li>
						<li class="menu__item menu__item_main menu-item jsOpenSubMenu">
							<a class="menu__item_expand" href="../services.php">Услуги</a>

							<ul class="menu__list-level menu__list-level_1">
								<li class="menu__item menu__item_level menu-item"><a href="#">Консалтинг в КНР</a></li>
								<li class="menu__item menu__item_level menu-item"><a href="#">Регистрация и открытие компании в кнр</a></li>
								<li class="menu__item menu__item_level menu-item jsOpenSubMenu">
									<a class="menu__item_level_expand" href="../marketing.php">Реклама и маркетинг в КНР</a>

									<ul class="menu__list-level menu__list-level_2">
										<li class="menu__item menu__item_level menu-item"><a href="#">Реклама в Baidu</a></li>
										<li class="menu__item menu__item_level menu-item"><a href="#">Реклама в WeChat</a></li>
										<li class="menu__item menu__item_level menu-item"><a href="#">Создание рекламного буклета на китайском языке</a></li>
										<li class="menu__item menu__item_level menu-item"><a href="#">Сайт в КНР</a></li>
									</ul>

								</li>
								<li class="menu__item menu__item_level menu-item"><a href="#">Выставки и форумы</a></li>
							</ul>

						</li>
						<li class="menu__item menu__item_main menu-item jsOpenSubMenu">
							<a class="menu__item_expand" href="../business.php">Бизнес с Китаем</a>

							<ul class="menu__list-level menu__list-level_1">
								<li class="menu__item menu__item_level menu-item jsOpenSubMenu">
									<a class="menu__item_level_expand" href="../market.php">КИТАЙСКИЙ РЫНОК</a>

									<ul class="menu__list-level menu__list-level_2">
										<li class="menu__item menu__item_level menu-item"><a href="#">ТОВАРЫ И ПРОДУКТЫ ШИРОКОГО ПОТРЕБЛЕНИЯ</a></li>
										<li class="menu__item menu__item_level menu-item"><a href="../page-business.php">АГРОБИЗНЕС</a></li>
										<li class="menu__item menu__item_level menu-item"><a href="#">РОЗНИЧНАЯ И ОПТОВАЯ ТОРГОВЛЯ</a></li>
										<li class="menu__item menu__item_level menu-item">ПРОМЫШЛЕННОЕ ПРОИЗВОДСТВО</li>
										<li class="menu__item menu__item_level menu-item">ГОРНОДОБЫВАЮЩАЯ ПРОМЫШЛЕННОСТЬ</li>
										<li class="menu__item menu__item_level menu-item">МЕТАЛЛУРГИЯ</li>
										<li class="menu__item menu__item_level menu-item">НЕФТЕГАЗОВАЯ ПРОМЫШЛЕННОСТЬ</li>
										<li class="menu__item menu__item_level menu-item">ХИМИЧЕСКАЯ ПРОМЫШЛЕННОСТЬ</li>
									</ul>

								</li>
								<li class="menu__item menu__item_level menu-item"><a href="#">ВЫСТАВКИ</a></li>
								<li class="menu__item menu__item_level menu-item"><a href="../marketing.php">ФОРУМЫ</a></li>
							</ul>

						</li>
						<li class="menu__item menu__item_main menu-item"><a href="../contacts.php">Контакты</a></li>
						<li class="menu__item menu__item_main menu-item"><a href="#">Бизнес Школа</a></li>
						<li class="menu__item menu__item_main menu-item"><a href="#">Фото и видео</a></li>
					</ul>
				</nav>
				<div class="header__panel">
					<div class="header__lang "><a href="#">РУС</a></div>
					<div class="header__search-icon jsOpenSearch">
						<div class="header__search-icon-wrap jsShowSearch">

							<div class="magnifier">
								<div class="magnifier__icon">
									<span class="magnifier__handle magnifier__handle_circle"></span>
									<span class="magnifier__handle magnifier__handle_grip"></span>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>

	</div>

	<div class="header__search search">
		<div class="container">
			<div class="row">

				<div class="search__input-wrap">
					<input type="text" class="search__input" placeholder="Поиск по ключевым словам ...">

					<div class="magnifier search__magnifier">
						<div class="magnifier__icon">
							<span class="magnifier__handle magnifier__handle_circle"></span>
							<span class="magnifier__handle magnifier__handle_grip"></span>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	<div class="header__contacts header__contacts_index">
		<div class="container">
			<div class="row">
				<div class="header__contacts-wrap header__contacts-wrap_index">
						<a class="header__contacts-item header__contacts-mail" href="mailto:pochta@gmail.com">pochta@gmail.com</a>
						<a class="header__contacts-item header__contacts-phone" href="tel:+78007894561">8 (800) 789-45-61</a>
						<a class="header__contacts-item header__contacts-btn button jsShowPopupCallback" href="#">Обратная связь</a>
				</div>
			</div>
		</div>
	</div>

</div>