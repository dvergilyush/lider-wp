<section class="section" id="press-releases">
	<div class="container">

		<div class="section__top press-releases__top">
			<h2 class="section__title">Пресс-Релизы</h2>
			<a href="<?php home_url(); ?>/pressreleases/all-releases/" class="btn-link section__link_desktop">
				<span class="btn-link__text uppercase">Все пресс-релизы</span>
				<span class="btn-link__icon"><?php include get_theme_file_path("parts/commons/icons/right-arrow.php")?></span>
			</a>
		</div>

		<div class="row">

			<?php
				$posts_count = wp_is_mobile() ? 1 : 2;
				// var_dump($posts_count);

				$posts = get_posts( array(
					'numberposts' => $posts_count,
					'orderby'     => 'date',
					'order'       => 'DESC',
					'post_type'   => 'releases',
					'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
				) );
				foreach( $posts as $post ){
					setup_postdata($post); ?>
					<div class="col-12 col-md-6 mb-mobile">
						<div class="card">
							<div class="card__body">
								<div class="card__date"><?php the_time('j F Y в H:i'); ?></div>
								<div class="card__title"><?php the_title(); ?></div>
								<div class="card__text card__text_pr"><?php the_excerpt(); ?></div>
								<a class="card__btn button" href="<?php the_permalink() ?>">Подробнее</a>
							</div>
						</div>
					</div>
				<?php }
				wp_reset_postdata(); // сброс
			?>

		</div>

		<a href="<?php home_url(); ?>/pressreleases/all-releases/" class="btn-link section__link_mobile">
			<span class="btn-link__text uppercase">Все пресс-релизы</span>
			<span class="btn-link__icon"><?php include get_theme_file_path("parts/commons/icons/right-arrow.php")?></span>
		</a>
	</div>
</section>