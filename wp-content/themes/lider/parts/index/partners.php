<section class="partners" id="partners">
	<div class="container">

		<div class="section__top">
			<h2 class="section__title">Партнеры Международного инновационного центра&nbsp;«Лидер»</h2>
			<a href="<?php home_url(); ?>/partners/" class="btn-link section__link_desktop">
				<span class="btn-link__text uppercase">Все партнеры</span>
				<span class="btn-link__icon"><?php include get_theme_file_path("parts/commons/icons/right-arrow.php")?></span>
			</a>
		</div>

		<div class="row">

			<?php
				// параметры по умолчанию
				$posts = get_posts( array(
					'numberposts' => 4,
					'category' => 'partners',
					'orderby'     => 'date',
					'order'       => 'ASC',
					'post_type'   => $post->post_type,
					'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
				) );
				foreach( $posts as $post ){
					setup_postdata($post); ?>
					<div class="col-6 col-md-3">
						<div class="partners__item">
							<div class="partners__logo_wrap">
								<img class="partners__logo" src="<?php the_post_thumbnail_url('medium'); ?>" alt="">
							</div>
							<div class="partners__name"><?php the_title(); ?></div>
							<div class="partners__descr"><?php the_excerpt(); ?></div>
						</div>
					</div>
				<?php }

					wp_reset_postdata(); // сброс
				?>
	</div>
</section>

