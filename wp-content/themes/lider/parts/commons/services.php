<div class="row">
	<?php 

		$terms = get_terms( array(
		'taxonomy'      => array( 'subservices' ), // название таксономии с WP 4.5
		'orderby'       => 'id', 
		'order'         => 'ASC',
		'exclude'          => 16,
		'hide_empty'    => false,
		) );

		if( $terms && ! is_wp_error($terms) ){ ?>

			<?php foreach( $terms as $term ){
					$image = get_field('background_image', 'subservices_' .$term->term_id);
					$icon = get_field('title_img', 'subservices_' .$term->term_id);
					$descr = $term->description;
					$description = mb_substr($descr, 0, 375) . "...";

			?>

				<div class="col-12 col-md-6 page__content-col">
					<div class="tile tile_size_big" style="background-image: url(<?php echo $image; ?>); background-size: cover; background-position: center;">
						<div class="tile__wrap overlay-bg relative">
							<div class="tile__title">
								<div class="tile__title-img-wrap tile__title-img-wrap_type_bg">
									<img src="<?php echo $icon; ?>" alt="" class="tile__img">
								</div>
								<div class="tile__title-text uppercase"><?php echo $term->name ?></div>
							</div>
							<div class="tile__text">
								<?php if ( $term->description !== '' ) { ?>
									<?php echo $description; ?>
								<?php } ?>
							</div>
							<a href="<?php echo get_term_link($term->term_id); ?>" class="btn-link tile__btn">
								<span class="btn-link__text uppercase">Подробнее</span>
								<span class="btn-link__icon"><?php include get_theme_file_path("parts/commons/icons/right-arrow.php")?></span>
							</a>
						</div>
					</div>
				</div>

				<?php }
			} ?>
</div>


<!-- /.row -->