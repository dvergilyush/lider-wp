<div  class="overlay-feedback overlay">
  <div class="popup">

    <div class="form form_modal">

      <div class="form__close jsClosePopup">
        <?php include get_theme_file_path("parts/commons/icons/cancel.php")?>
      </div>

      <?php echo do_shortcode('[contact-form-7 id="250" title="Contact form Modal"]')?>
  </div>

  </div>
</div>

<div id="popupThx"  class="overlay-success overlay">
  <div class="popup-success">
<!--     <div class="form__close jsClosePopup">
      <?php include get_theme_file_path("parts/commons/icons/cancel.php")?>
    </div> -->

    <div class="popup-success__wrap">
      <div class="form__title uppercase">Форма отправлена</div>
      <div class="form__subtitle">Мы свяжемся с Вами в течение 1 часа</div>
      <button type="submit" class="form__button form__button button jsClosePopup">Закрыть</button>
    </div>

  </div>
</div>
