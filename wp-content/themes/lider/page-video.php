<?php get_header();?>
<style>
    .page__content .col-12.col-md-6 iframe {
        max-width: 100% !important;
    }
</style>

<section class="video page__category" id="video" >
    <div class="container">
        
        <h2 class="page__title"><?php echo wp_get_document_title(); ?></h2>
        <div class="row page__subtitle">
            <?php
                if ( function_exists('yoast_breadcrumb') ) {
                    yoast_breadcrumb('<p id="breadcrumbs">','</p>');
                }
            ?>
        </div>
        <div class="page__content">
            <div class="page__text">
                <div class="row">
                    <?php 
                        $videos = get_field('videos');
                        $videos = preg_split("/[\s,]+/", $videos);
                
                        foreach( $videos as $key => $video){
                            preg_match('/\/(.{11})$/' , $video, $m);
                            $videos[$key] = $m[1];
                        }
                     ?>
                     <?php 
                        foreach ($videos as $key => $video) { ?>
                            <div class="col-12 col-md-6">
                                <div class="video__item overlay-bg relative">
                                    <iframe src="https://www.youtube.com/embed/<?= $video ?>?rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                            </div>
                            
                        <?php }
                      ?>
                </div>
                <!-- /.row --></div>
            <div class="page__text">
                <?php the_content(); ?>
            </div>
        </div>
        <!-- /.page__content -->
    </div>
    <!-- /.container -->
</section>

<?php get_footer();?>
