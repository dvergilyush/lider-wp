<form method="get" action="<?php echo home_url( '/' ); ?>" class="search__input-wrap">
    <input value="" name="s" id="s" type="text" class="search__input" placeholder="Поиск по ключевым словам ...">

    <button type="submit" class="magnifier search__magnifier">
        <div class="search__magnifier-icon magnifier__icon">
            <span class="magnifier__handle magnifier__handle_circle"></span>
            <span class="magnifier__handle magnifier__handle_grip"></span>
        </div>
    </button>
</form>