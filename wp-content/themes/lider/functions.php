<?php

add_action('wp_enqueue_scripts', 'lider_styles');
add_action('wp_enqueue_scripts', 'lider_scripts');
add_action('after_setup_theme', 'myMenu');


//стили
function lider_styles()
{
    wp_enqueue_style('main-style', get_stylesheet_uri());
    wp_enqueue_style('main-style-2', get_template_directory_uri() . '/css/common.min.css');
}

//скрипты
function lider_scripts()
{
	wp_deregister_script('jquery');
	wp_enqueue_script('main-script', get_template_directory_uri() . '/js/scripts.min.js', [], false, true);
	wpcf7_enqueue_scripts(['main-script']);
}

function myMenu()
{
    register_nav_menu('top', 'Для шапки');
}

add_theme_support('post-thumbnails');
add_theme_support('title-tag');
add_theme_support('post-formats', array('aside', 'quote'));
add_theme_support( 'html5', array( 'search-form' ) );

// add_image_size('intermediate', 480, 300, true);


// Изменяет основные параметры меню
add_filter( 'wp_nav_menu_args', 'filter_wp_menu_args' );
function filter_wp_menu_args( $args ) {
    if ( $args['theme_location'] === 'top' ) {
        $args['container']  = false;
        $args['items_wrap'] = '<ul class="%2$s">%3$s</ul>';
        $args['menu_class'] = 'menu__list flex-nowrap justify-content-between';
        $args['menu_id'] = '';

    }
    return $args;
}

// Изменяем атрибут id у тега li
add_filter('nav_menu_item_id', 'filter_menu_item_css_id', 10, 4);
function filter_menu_item_css_id($menu_id, $item, $args, $deps){
    return $args->theme_location === 'top' ? '' : $menu_id;
}

// Изменяем атрибут class у тега li
add_filter( 'nav_menu_css_class', 'filter_nav_menu_css_classes', 10, 4 );
function filter_nav_menu_css_classes( $classes, $item, $args, $depth ) {
    if ( $args->theme_location === 'top' ) {
        $classes = [ 'menu__item'];
        if ( $item->current ) {

        }
        if ( $item->menu_item_parent === '0' ) {
            $classes[] = 'menu__item_main';
        } else {
            $classes[] = 'menu__item_level';
        }
        if (in_array('menu-item-has-children', $item->classes)) {
            $classes[] = 'jsOpenSubMenu';
        }
    }
    return $classes;
}
// Изменяет класс у вложенного ul
add_filter( 'nav_menu_submenu_css_class', 'filter_nav_menu_submenu_css_class', 10, 3 );
function filter_nav_menu_submenu_css_class( $classes, $args, $depth ) {
    if ( $args->theme_location === 'top' ) {
        $classes = [
            'menu__list-level',
            'menu__list-level_' . ( $depth + 1),
        ];
    }
    return $classes;
}
// Добавляем классы ссылкам
add_filter( 'nav_menu_link_attributes', 'filter_nav_menu_link_attributes', 10, 4 );
function filter_nav_menu_link_attributes( $atts, $item, $args, $depth ) {
    if ( $args->theme_location === 'top' ) {
        $atts['class'] = 'menu__item-link ';
	    if ( $item->menu_item_parent === '0' ) {
		    $atts['class'] .= 'menu__item-link_main ';
	    } else {
		    $atts['class'] .= 'menu__item-link_level ';
	    }
        if ( in_array('menu-item-has-children', $item->classes) ) {
            if ( $item->menu_item_parent === '0' ) {
                $atts['class'] .= 'menu__item_expand ';
            } else {
                $atts['class'] .= 'menu__item_level_expand';
            }
        }
        if ( $item->current ) {

        }
    }
    return $atts;
}

add_theme_support('custom-logo');

// Регистрируем виджеты для шапки и подвала
add_action( 'widgets_init', 'register_footer_links' );
function register_footer_links()
{
	register_sidebar( array(
		'name' => "Разделы со ссылками в подвале",
		'id' => 'footer-sidebar_links',
		'description' => 'Эти виджеты будут показаны в блоке со ссылками на разделы сайта',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '',
		'after_title' => ''
	) );
}

add_action( 'widgets_init', 'register_footer_text' );
function register_footer_text()
{
	register_sidebar( array(
		'name' => "Блок текста в подвале",
		'id' => 'footer-sidebar_text',
		'description' => 'Этот блок текста будет показан в подвале',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '',
		'after_title' => ''
	) );
}



// Регистрируем произвольные типы страниц и таксономии
add_action('init', 'register_services_pages');
function register_services_pages()
{
	register_post_type('services', array(
		'label'  => null,
		'labels' => array(
			'name'               => 'Услуги', // Основное название типа записи
			'singular_name'      => 'Услуга', // отдельное название записи типа 
			'add_new'            => 'Добавить новую',
			'add_new_item'       => 'Добавить новую услугу',
			'edit_item'          => 'Редактировать услугу',
			'new_item'           => 'Новая услуга',
			'view_item'          => 'Посмотреть услугу',
			'search_items'       => 'Найти услугу',
			'not_found'          =>  'Услуг не найдено',
			'not_found_in_trash' => 'В корзине услуг не найдено',
			'parent_item_colon'  => '',
			'menu_name'          => 'Услуги'
		),
		'description'         => '',
		'public'              => true,
		'publicly_queryable'  => true, // зависит от public
		'exclude_from_search' => false, // зависит от public
		'show_ui'             => true, // зависит от public
		'show_in_menu'        => true, // показывать ли в меню адмнки
		'show_in_admin_bar'   => true, // по умолчанию значение show_in_menu
		'show_in_nav_menus'   => true, // зависит от public
		'show_in_rest'        => true, // добавить в REST API. C WP 4.7
		'rest_base'           => null, // $post_type. C WP 4.7
		'menu_position'       => 4,
		'menu_icon'           => 'dashicons-products', 
		'capability_type'   => 'post',
		//'capabilities'      => 'post', // массив дополнительных прав для этого типа записи
		//'map_meta_cap'      => null, // Ставим true чтобы включить дефолтный обработчик специальных прав
		'hierarchical'        => false,
		'supports'            => array('title','editor', 'thumbnail', 'custom-fields'), // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
		'taxonomies'          => array('subservices'),
		'has_archive'         => false,
		'rewrite'             => true,
		'query_var'           => true,
	) );
}

// хук для регистрации
add_action('init', 'create_subservices_taxonomy');
function create_subservices_taxonomy()
{
	register_taxonomy('subservices', array('services'), array(
		'label'                 => '', // определяется параметром $labels->name
		'labels'                => array(
			'name'              => 'Подразделы услуг',
			'singular_name'     => 'Подраздел услуг',
			'search_items'      => 'Найти подраздел услуг',
			'all_items'         => 'Все подразделы услуг',
			'view_item '        => 'Смотреть подразделы услуг',
			'parent_item'       => 'Родительский подраздел услуг',
			'parent_item_colon' => 'Родительский подраздел услуг:',
			'edit_item'         => 'Редактировать подраздел услуг',
			'update_item'       => 'Обновить подраздел услуг',
			'add_new_item'      => 'Добавить подраздел услуг',
			'new_item_name'     => 'Новое имя подраздела услуг',
			'menu_name'         => 'Подразделы услуг',
		),
		'description'           => 'Все подразделы услуг', // описание таксономии
		'public'                => true,
		'publicly_queryable'    => null, // равен аргументу public
		'hierarchical'          => true,
		'rewrite'               => true,

	) );
}

add_action('init', 'register_article_pages');
function register_article_pages()
{
	register_post_type('article', array(
		'label'  => null,
		'labels' => array(
			'name'               => 'Бизнес с Китаем', // Основное название типа записи
			'singular_name'      => 'Статья', // отдельное название записи типа 
			'add_new'            => 'Добавить новую статью',
			'add_new_item'       => 'Добавить новую статью',
			'edit_item'          => 'Редактировать статью',
			'new_item'           => 'Новая статья',
			'view_item'          => 'Посмотреть статью',
			'search_items'       => 'Найти статью',
			'not_found'          =>  'Статей не найдено',
			'not_found_in_trash' => 'В корзине статей не найдено',
			'parent_item_colon'  => '',
			'menu_name'          => 'Бизнес с Китаем'
		),
		'description'         => '',
		'public'              => true,
		'publicly_queryable'  => true, // зависит от public
		'exclude_from_search' => false, // зависит от public
		'show_ui'             => true, // зависит от public
		'show_in_menu'        => true, // показывать ли в меню адмнки
		'show_in_admin_bar'   => true, // по умолчанию значение show_in_menu
		'show_in_nav_menus'   => true, // зависит от public
		'show_in_rest'        => true, // добавить в REST API. C WP 4.7
		'rest_base'           => null, // $post_type. C WP 4.7
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-welcome-add-page', 
		'capability_type'   => 'post',
		//'capabilities'      => 'post', // массив дополнительных прав для этого типа записи
		//'map_meta_cap'      => null, // Ставим true чтобы включить дефолтный обработчик специальных прав
		'hierarchical'        => false,
		'supports'            => array('title','editor', 'thumbnail', 'custom-fields'), // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
		'taxonomies'          => array(''),
		'has_archive'         => false,
		'rewrite'             => true,
		'query_var'           => true,
	) );
}

// хук для регистрации
add_action('init', 'create_business_taxonomy');
function create_business_taxonomy()
{
	register_taxonomy('business', array('article'), array(
		'label'                 => '', // определяется параметром $labels->name
		'labels'                => array(
			'name'              => 'Подразделы Бизнеса с Китаем',
			'singular_name'     => 'Подраздел Бизнеса с Китаем',
			'search_items'      => 'Найти подраздел Бизнеса с Китаем',
			'all_items'         => 'Все подразделы Бизнеса с Китаем',
			'view_item '        => 'Смотреть подразделы Бизнеса с Китаем',
			'parent_item'       => 'Родительский подраздел Бизнеса с Китаем',
			'parent_item_colon' => 'Родительский подраздел Бизнеса с Китаем:',
			'edit_item'         => 'Редактировать подраздел Бизнеса с Китаем',
			'update_item'       => 'Обновить подраздел Бизнеса с Китаем',
			'add_new_item'      => 'Добавить подраздел Бизнеса с Китаем',
			'new_item_name'     => 'Новое имя подраздела Бизнеса с Китаем',
			'menu_name'         => 'Подразделы Бизнеса с Китаем',
		),
		'description'           => 'Все подразделы Бизнеса с Китаем', // описание таксономии
		'public'                => true,
		'publicly_queryable'    => null, // равен аргументу public
		'hierarchical'          => true,
		'rewrite'               => true,

	) );
}

add_action('init', 'register_pressreleases_pages');

function register_pressreleases_pages()
{
	register_post_type('releases', array(
		'label'  => null,
		'labels' => array(
			'name'               => 'Пресс-релизы', // Основное название типа записи
			'singular_name'      => 'Пресс-релиз', // отдельное название записи типа
			'add_new'            => 'Добавить новый',
			'add_new_item'       => 'Добавить новый пресс-релиз',
			'edit_item'          => 'Редактировать пресс-релиз',
			'new_item'           => 'Новый пресс-релиз',
			'view_item'          => 'Посмотреть пресс-релиз',
			'search_items'       => 'Найти пресс-релиз',
			'not_found'          =>  'Пресс-релизов не найдено',
			'not_found_in_trash' => 'В корзине пресс-релизов не найдено',
			'parent_item_colon'  => '',
			'menu_name'          => 'Пресс-релизы'
		),
		'description'         => '',
		'public'              => true,
		'publicly_queryable'  => true, // зависит от public
		'exclude_from_search' => false, // зависит от public
		'show_ui'             => true, // зависит от public
		'show_in_menu'        => true, // показывать ли в меню адмнки
		'show_in_admin_bar'   => true, // по умолчанию значение show_in_menu
		'show_in_nav_menus'   => true, // зависит от public
		'show_in_rest'        => true, // добавить в REST API. C WP 4.7
		'rest_base'           => null, // $post_type. C WP 4.7
		'menu_position'       => 6,
		'menu_icon'           => 'dashicons-format-quote',
		'capability_type'   => 'post',
		//'capabilities'      => 'post', // массив дополнительных прав для этого типа записи
		//'map_meta_cap'      => null, // Ставим true чтобы включить дефолтный обработчик специальных прав
		'hierarchical'        => false,
		'supports'            => array('title','editor', 'thumbnail', 'custom-fields'), // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
		'taxonomies'          => array('pressreleases'),
		'has_archive'         => false,
		'rewrite'             => true,
		'query_var'           => true,
	) );
}

// хук для регистрации
add_action('init', 'create_pressreleases_taxonomy');
function create_pressreleases_taxonomy()
{
	register_taxonomy('pressreleases', array('releases'), array(
		'label'                 => '', // определяется параметром $labels->name
		'labels'                => array(
			'name'              => 'Рубрики пресс-релизов',
			'singular_name'     => 'Рубрика пресс-релизов',
			'search_items'      => 'Найти рубрику пресс-релизов',
			'all_items'         => 'Все рубрики пресс-релизов',
			'view_item '        => 'Смотреть рубрику пресс-релизов',
			'parent_item'       => 'Родительская рубрика пресс-релизов',
			'parent_item_colon' => 'Родительская рубрика пресс-релизов:',
			'edit_item'         => 'Редактировать рубрику пресс-релизов',
			'update_item'       => 'Обновить рубрику пресс-релизов',
			'add_new_item'      => 'Добавить рубрику пресс-релизов',
			'new_item_name'     => 'Новое имя рубрики пресс-релизов',
			'menu_name'         => 'Рубрики пресс-релизов',
		),
		'description'           => 'Все рубрики пресс-релизов', // описание таксономии
		'public'                => true,
		'publicly_queryable'    => null, // равен аргументу public
		'hierarchical'          => true,
		'rewrite'               => true,

	) );
}

add_action('init', 'register_news_pages');

function register_news_pages()
{
	register_post_type('news', array(
		'label'  => null,
		'labels' => array(
			'name'               => 'Новости', // Основное название типа записи
			'singular_name'      => 'Новость', // отдельное название записи типа
			'add_new'            => 'Добавить новость',
			'add_new_item'       => 'Добавить новость',
			'edit_item'          => 'Редактировать новость',
			'new_item'           => 'Новая новость',
			'view_item'          => 'Посмотреть новость',
			'search_items'       => 'Найти новость',
			'not_found'          =>  'Новостей не найдено',
			'not_found_in_trash' => 'В корзине новостей не найдено',
			'parent_item_colon'  => '',
			'menu_name'          => 'Новости'
		),
		'description'         => '',
		'public'              => true,
		'publicly_queryable'  => true, // зависит от public
		'exclude_from_search' => false, // зависит от public
		'show_ui'             => true, // зависит от public
		'show_in_menu'        => true, // показывать ли в меню адмнки
		'show_in_admin_bar'   => true, // по умолчанию значение show_in_menu
		'show_in_nav_menus'   => true, // зависит от public
		'show_in_rest'        => true, // добавить в REST API. C WP 4.7
		'rest_base'           => null, // $post_type. C WP 4.7
		'menu_position'       => 6,
		'menu_icon'           => 'dashicons-admin-site',
		'capability_type'   => 'post',
		//'capabilities'      => 'post', // массив дополнительных прав для этого типа записи
		//'map_meta_cap'      => null, // Ставим true чтобы включить дефолтный обработчик специальных прав
		'hierarchical'        => false,
		'supports'            => array('title','editor', 'thumbnail', 'custom-fields'), // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
		'taxonomies'          => array('news-rubric'),
		'has_archive'         => false,
		'rewrite'             => true,
		'query_var'           => true,
	) );
}

// хук для регистрации
add_action('init', 'create_news_taxonomy');
function create_news_taxonomy()
{
	register_taxonomy('news-rubric', array('news'), array(
		'label'                 => '', // определяется параметром $labels->name
		'labels'                => array(
			'name'              => 'Рубрики новостей',
			'singular_name'     => 'Рубрика новостей',
			'search_items'      => 'Найти рубрику новостей',
			'all_items'         => 'Все рубрики новостей',
			'view_item '        => 'Смотреть рубрику новостей',
			'parent_item'       => 'Родительская рубрика новостей',
			'parent_item_colon' => 'Родительская рубрика новостей:',
			'edit_item'         => 'Редактировать рубрику новостей',
			'update_item'       => 'Обновить рубрику новостей',
			'add_new_item'      => 'Добавить рубрику новостей',
			'new_item_name'     => 'Новое имя рубрики новостей',
			'menu_name'         => 'Рубрики новостей',
		),
		'description'           => 'Все рубрики новостей', // описание таксономии
		'public'                => true,
		'publicly_queryable'    => null, // равен аргументу public
		'hierarchical'          => true,
		'rewrite'               => true,

	) );
}

add_filter( 'excerpt_length', function()
{
	return 16;
} );

// удаляет H2 из шаблона пагинации
add_filter('navigation_markup_template', 'my_navigation_template', 10, 2 );
function my_navigation_template( $template, $class ){
    /*
    Вид базового шаблона:
    <nav class="navigation %1$s" role="navigation">
        <h2 class="screen-reader-text">%2$s</h2>
        <div class="nav-links">%3$s</div>
    </nav>
    */

    return '
	<nav class="navigation %1$s" role="navigation">
		<div class="nav-links pagination__wrap">%3$s</div>
	</nav>    
	';
}

function getContent ($path)
{
    ob_start();
    include $path;
    return ob_get_clean();
}

// выводим пагинацию
//the_posts_pagination( array(
//    'end_size' => 2,
//) );

// Переделаем checkbox в radio на страницах записей
add_action( 'admin_print_footer_scripts', 'func_hook_admin_footer_scripts', 99 );
function func_hook_admin_footer_scripts()
{
	if( get_current_screen()->base !== 'post' )
		return;
	?>
	<script>
		[ 'subservices', 'business' ].forEach(function(taxname){
			jQuery( '#' + taxname + 'div input[type="checkbox"]' ).prop( 'type', 'radio' );
		})
	</script>
	<?php
}

// Скрываем ненужные пункты в боковой панели админки
add_action('admin_menu', 'remove_menus');
function remove_menus()
{
	global $menu;
	$restricted = array(
		__('Dashboard'),
		// __('Posts'),
		__('Media'),
		__('Links'),
		__('Tools'),
		__('Users'),
		__('Comments'),
	);
	end ($menu);
	while (prev($menu)){
		$value = explode(' ', $menu[key($menu)][0]);
		if( in_array( ($value[0] != NULL ? $value[0] : "") , $restricted ) ){
			unset($menu[key($menu)]);
		}
	}
}

function the_excerpt_max_charlength( $content, $charlength ){
    $result = '';

	$excerpt = $content;
	$charlength++;

	if ( mb_strlen( $excerpt ) > $charlength ) {
		$subex = mb_substr( $excerpt, 0, $charlength - 5 );
		$exwords = explode( ' ', $subex );
		$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );


		if ( $excut < 0 ) {
			$result .=  mb_substr( $subex, 0, $excut );
		} else {
			$result .= $subex;
		}
		$result .= '...';
	} else {
		$result .= $excerpt;
	}

	return $result;
}