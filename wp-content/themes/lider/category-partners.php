<?php get_header(); ?>
<section class="partners page__category">

    <div class="container">
        <h2 class="page__title"><?php single_term_title(); ?></h2>

		<div class="row page__subtitle">
			<?php
				if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				}
			?>
		</div>
		
		<div class="page__content">
			<?php if ( term_description() !== '' ) { ?>
				<div class="page__text"><?php echo term_description(); ?></div>
			<?php } ?>

			<div class="row">

				<?php global $query_string; // параметры базового запроса
				query_posts( $query_string.'&order=ASC'); ?>

				<?php if ( have_posts() ) :  while ( have_posts() ) : the_post(); ?>
					<div class="col-6 col-md-3">
						<div class="partners__item">
							<div class="partners__logo_wrap">
								<img class="partners__logo" src="<?php the_post_thumbnail_url('medium'); ?>" alt="">
							</div>
							<div class="partners__name"><?php the_title(); ?></div>
							<div class="partners__descr"><?php the_excerpt(); ?></div>
						</div>
					</div>
				<?php endwhile; ?>
				<?php endif; ?>

				<?php wp_reset_query(); ?>

				

			</div>

		</div>


		<?php echo get_the_posts_pagination(array(
		    'show_all' => false, // показаны все страницы участвующие в пагинации
		    'end_size' => 1,     // количество страниц на концах
		    'mid_size' => 1,     // количество страниц вокруг текущей
		    'prev_next' => true,  // выводить ли боковые ссылки "предыдущая/следующая страница".
		    'prev_text' => getContent("parts/commons/icons/arrow-point-to-right.php"),
		    'next_text' => getContent("parts/commons/icons/arrow-point-to-right.php"),
		    'add_args' => false, // Массив аргументов (переменных запроса), которые нужно добавить к ссылкам.
		    'add_fragment' => '',     // Текст который добавиться ко всем ссылкам.
		    'screen_reader_text' => __('Posts navigation'),
		)); ?>


	</div>
</section>


<?php get_footer(); ?>
