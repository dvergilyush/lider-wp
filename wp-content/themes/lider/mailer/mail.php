<?php
header("Content-Type: text/html; charset=utf-8;");

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require '../../vendor/autoload.php';

$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
try {
    //Server settings
    $mail->SMTPDebug = 2;                                 // Enable verbose debug output
$mail->CharSet = 'UTF-8';
$mail->setLanguage('ru');
$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'smtp.yandex.ru';  // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'saran10@yandex.ru';                 // SMTP username
$mail->Password = '5wiH%bLJx!';                           // SMTP password
$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = 587;                                    // TCP port to connect to

//Recipients
$mail->setFrom('saran10@yandex.ru', 'Mailer');
$mail->addAddress('alex_saranin@list.ru', 'Alex');     // Add a recipient

//Content
$mail->isHTML(true);                                  // Set email format to HTML
$mail->Subject = 'Новая заявка с сайта';

$data = [];
$data['name'] = $_POST['name'];
$data['email'] = $_POST['email'];
$data['phone'] = $_POST['phone'];
$data['msg'] = $_POST['message'];

ob_start();

include('mail_tpl.php');

$body = ob_get_contents();

ob_end_clean ();

$mail->Body    = $body;
$mail->AltBody = $body;

$mail->send();
echo 'Message has been sent';
} catch (Exception $e) {
echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
}