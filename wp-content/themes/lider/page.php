<?php get_header();?>

<section class="about page__category">

    <div class="container">
        <h2 class="page__title"><?php echo wp_get_document_title(); ?></h2>
        <div class="row page__subtitle">
            <?php
                if ( function_exists('yoast_breadcrumb') ) {
                    yoast_breadcrumb('<p id="breadcrumbs">','</p>');
                }
            ?>
        </div>

        <div class="page__content">
            <div class="page__text">
                <?php the_content(); ?>
            </div>
        </div>
    </div>
</section>

<?php get_footer();?>
