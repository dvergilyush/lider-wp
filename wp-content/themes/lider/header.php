<?php include 'parts/header/head.php'?>

<body>

<div class="header">

    <div class="header__line">

        <div class="container relative">

            <div class="header__burger jsShowMenu">
                <div class="burger burger_dark">
                    <span></span>
                </div>
            </div>


            <div class="row flex-nowrap justify-content-between">
                <nav class="header__menu menu nav-menu jsMenu">
                    <?php wp_nav_menu(['theme_location' => 'top']); ?>
                </nav>
                <div class="header__panel">
                    <div class="header__lang "><a href="#">РУС</a></div>
                    <div class="header__search-icon jsOpenSearch">
                        <div class="header__search-icon-wrap jsShowSearch">

                            <div class="magnifier">
                                <div class="magnifier__icon">
                                    <span class="magnifier__handle magnifier__handle_circle"></span>
                                    <span class="magnifier__handle magnifier__handle_grip"></span>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="header__search search">
        <div class="container">
            <div class="row">
                <?php get_search_form(); ?>
            </div>
        </div>
    </div>
    <div class="header__contacts">
        <div class="container">
            <div class="row">
                <div class="header__contacts-wrap">
                    <a class="header__contacts-item header__contacts-mail" href="mailto:<?php the_field('contacts-block_email', 68);?>"><?php the_field('contacts-block_email', 68);?></a>
                    <a class="header__contacts-item header__contacts-phone" href="tel:<?php the_field('contacts-block_phone', 68);?>"><?php the_field('contacts-block_phone', 68);?></a>
                    <a class="header__contacts-item header__contacts-btn button jsShowPopupCallback" href="#">Обратная
                        связь</a>
                </div>
            </div>
        </div>
    </div>

</div>