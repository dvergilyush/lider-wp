		<footer class="footer site-footer">
			<div class="container">
				<div class="footer__wrap">

					<div class="footer__block footer__policy">
						<div class="footer__text footer__policy-par">
							<?php
								if ( function_exists('dynamic_sidebar') )
									dynamic_sidebar('footer-sidebar_text');
							?>
						</div>
						<div class="footer__text footer__link underline">
							<a href="http://leader-consalt.ru/privacy-policy">Политика конфиденциальности</a>
						</div>
					</div>
					
					<div class="footer__block footer__sections">
						<?php
							if ( function_exists('dynamic_sidebar') )
								dynamic_sidebar('footer-sidebar_links');
						?>

					</div>
					<div class="footer__block footer__contacts">

						<div class="footer__contact-wrap">
							<div class="footer__contact">
								<div class="footer__contacts-label footer__text">телефон:</div>
								<div class="footer__contacts-data"><?php the_field('contacts-block_phone', 68);?></div>
								<div class="footer__contacts-link footer__text underline">
									<a href="tel:<?php the_field('contacts-block_phone', 68);?>">позвонить</a>
								</div>
							</div>
							<div class="footer__contact">
								<div class="footer__contacts-label footer__text">e-mail:</div>
								<div class="footer__contacts-data"><?php the_field('contacts-block_email', 68);?></div>
								<div class="footer__contacts-link footer__text underline">
									<a href="mailto:<?php the_field('contacts-block_email', 68);?>">написать письмо</a></div>
							</div>
							<div class="footer__contact">
								<div class="footer__contacts-label footer__text">адрес:</div>
								<div class="footer__contacts-data"><?php the_field('contacts-block_address', 68);?></div>
								<div class="footer__contacts-link footer__text underline">
									<a href="#">показать на карте</a></div>
							</div>
						</div>


						<div class="footer__socials">
							<a href="<?php the_field('vk', 68)?>">
								<div class="footer__socials-item social-icon">
									<?php include get_theme_file_path("parts/commons/icons/vk.php")?>
								</div>
							</a>
							<a href="<?php the_field('fb', 68)?>">
								<div class="footer__socials-item social-icon">
									<?php include get_theme_file_path("parts/commons/icons/facebook.php")?>
								</div>
							</a>
							<a href="<?php the_field('inst', 68)?>">
								<div class="footer__socials-item social-icon">
									<?php include get_theme_file_path("parts/commons/icons/instagram.php")?>
								</div>
							</a>
							<a href="<?php the_field('youtube', 68)?>">
								<div class="footer__socials-item social-icon">
									<?php include get_theme_file_path("parts/commons/icons/youtube.php")?>
								</div>
							</a>
							<a href="<?php the_field('chat', 68)?>">
								<div class="footer__socials-item social-icon">
									<?php include get_theme_file_path("parts/commons/icons/wechat.php")?>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<?php include get_theme_file_path('parts/commons/modals.php'); ?>
		<?php wp_footer(); ?>
	</body>

</html>