<?php get_header('home'); ?>

<?php include 'parts/index/main.php'; ?>
<?php include 'parts/index/press-releases.php'; ?>

<section class="section services" id="services">
  <div class="container">
    <div class="section__top">
      <h2 class="section__title">Услуги Международного инновационного центра «Лидер»</h2>
    </div>
    <?php include 'parts/commons/services.php'; ?>

  </div>
</section>

<?php include 'parts/index/business.php'; ?>
<?php include 'parts/index/partners.php'; ?>

<?php get_footer(); ?>
