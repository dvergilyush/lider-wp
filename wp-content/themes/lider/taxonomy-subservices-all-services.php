<?php get_header();?>

<section class="services page__category">

    <div class="container">
        <h2 class="page__title"><?php echo wp_get_document_title(); ?></h2>
        <div class="row page__subtitle">
            <?php
                if ( function_exists('yoast_breadcrumb') ) {
                    yoast_breadcrumb('<p id="breadcrumbs">','</p>');
                }
            ?>
        </div>
        <div class="page__content page__content_padding-bottom">
            <?php include 'parts/commons/services.php';?>
        </div>
        

    </div>
</section>

<?php get_footer();?>
