<?php get_header();?>

<section class="business page__category" id="business" >
    <div class="container">
        <h2 class="page__title"><?php echo wp_get_document_title(); ?></h2>
        <div class="row page__subtitle">
            <?php
                if ( function_exists('yoast_breadcrumb') ) {
                    yoast_breadcrumb('<p id="breadcrumbs">','</p>');
                }
            ?>
        </div>
        <div class="page__content page__content_padding-bottom">
            <div class="row">
                <?php

                    $terms = get_terms( array(
                    'taxonomy'      => array( 'business' ), // название таксономии с WP 4.5
                    'orderby'       => 'id',
                    'order'         => 'ASC',
                    'exclude'          => 15,
                    'hide_empty'    => false,
                    ) );

                    if( $terms && ! is_wp_error($terms) ){ ?>

                        <?php foreach( $terms as $term ){
                            $image = get_field('background_image', 'business_' .$term->term_id);
                            $descr = $term->description;
                            $description = mb_substr($descr, 0, 300) . "...";

                        ?>

                        <div class="col-12 col-md-4">
                            <div class="business__item business__item_page business__item_secondary overlay-bg relative" style="background-image: url(<?php echo $image; ?>); background-size: cover; background-position: center;">
                                <div class="business__title uppercase"><?php echo $term->name ?></div>
                                <a href="<?php echo get_term_link($term->term_id); ?>" class="btn-link">
                                    <span class="btn-link__text uppercase">Подробнее</span>
                                    <span class="btn-link__icon"><?php include get_theme_file_path("parts/commons/icons/right-arrow.php")?></span>
                                </a>
                            </div>
                        </div>

                        <?php }
                    } ?>
            </div>
        </div>
    </div>
    <!-- /.container -->
</section>

<?php get_footer();?>
