<?php get_header(); ?>
<section class="marketing page__category">

	<div class="container">
		<h2 class="page__title"><?php single_term_title(); ?></h2>

		<div class="row page__subtitle">
			<?php
				if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				}
			?>
		</div>
		
		<div class="page__content">
			<?php if ( term_description() !== '' ) { ?>
				<div class="page__text"><?php echo term_description(); ?></div>
			<?php } ?>

		<div class="row">
			<?php global $query_string; // параметры базового запроса
			
			if (wp_is_mobile()) {
				query_posts($query_string.'&posts_per_page=3&post_type=releases');
			} else {
				query_posts($query_string.'&posts_per_page=6&post_type=releases');
			}
			?>

			<?php if ( have_posts() ) :  while ( have_posts() ) : the_post(); ?>
				<div class="col-12 col-md-6 col-lg-4 page__content-col">
					<div class="card">
						<div class="card__body">
							<div class="card__date"><?php the_time('j F Y в H:i'); ?></div>
							<div class="card__title"><?php the_title(); ?></div>
							<div class="card__text card__text_pr"><?php the_excerpt(); ?></div>
							<a class="card__btn button" href="<?php the_permalink() ?>">Подробнее</a>
						</div>
					</div>
				</div>
			<?php endwhile; ?>
			<?php endif; ?>

			<?php wp_reset_query(); ?>

		</div>

		</div>

		<?php echo get_the_posts_pagination(array(
		    'show_all' => false, // показаны все страницы участвующие в пагинации
		    'end_size' => 1,     // количество страниц на концах
		    'mid_size' => 1,     // количество страниц вокруг текущей
		    'prev_next' => true,  // выводить ли боковые ссылки "предыдущая/следующая страница".
		    'prev_text' => getContent("parts/commons/icons/arrow-point-to-right.php"),
		    'next_text' => getContent("parts/commons/icons/arrow-point-to-right.php"),
		    'add_args' => false, // Массив аргументов (переменных запроса), которые нужно добавить к ссылкам.
		    'add_fragment' => '',     // Текст который добавиться ко всем ссылкам.
		    'screen_reader_text' => __('Posts navigation'),
		)); ?>

	</div>
</section>


<?php get_footer(); ?>
