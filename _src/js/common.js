$(document).ready(function () {


    var magnifier = $('.jsShowSearch');
    magnifier.on('click', function () {
        $(this).find('.magnifier__icon').toggleClass('on');
        $('.search').slideToggle();
    });


    var Body = $('body');
    var Burger = $('.burger');
    var Menu = $('.jsMenu');

    // Body.toggleClass('body_no-scroll');

    function menu() {
        $('.jsOpenSubMenu').on('click', function (e) {
            var Item = $(this);
            var curMenu = Item.closest('ul');
            var curMenuHeight = curMenu.outerHeight();
            var nextMenu = Item.children('.menu__list-level');
            nextMenu.addClass('menu__list-level_open');
            var nextMenuHeight = nextMenu.outerHeight();
            console.log(nextMenuHeight);
            console.log(curMenuHeight);
            if (nextMenuHeight < curMenuHeight) {
                nextMenu.height(curMenuHeight);
            }
            curMenu.css('overflow', 'hidden');
        });
        // Menu
        $(".jsShowMenu").click(function () {

            Burger.toggleClass('on');
            Body.toggleClass('body_no-scroll');
            Menu.fadeToggle({
                complete: function () {
                    if (!Burger.hasClass('on')) {
                        Menu.find('.menu__list-level_open').removeClass('menu__list-level_open');
                    }
                }
            });

        });
    }

    // if (window.matchMedia("(max-width: 992px)").matches) {
    // 	Menu.hide();
    // } else {
    // 	Body.removeClass('body_no-scroll');
    // 	Burger.removeClass('on');
    // 	Menu.show();
    // }

    menu();

    function setup_for_width(mql) {
        if (mql.matches) {
            Body.removeClass('body_no-scroll');
            Burger.removeClass('on');
            Menu.hide();

            setTimeout(function () {
                $('.slick-dots li button').append('<span>');
            }, 300);

            $('.menu__item_level_expand, .menu__item_expand').on('click', function (e) {
                e.preventDefault();
            });

        } else {
            Menu.show();
            setTimeout(function () {
                $('.slick-dots li button').append('<span>');
            }, 300);
        }

    }

    var mql = window.matchMedia("(max-width: 992px)");
    mql.addListener(setup_for_width); // Добавим прослушку на смену результата
    setup_for_width(mql); // Вызовем нашу функцию

    // Модальное окно
    $('.jsShowPopupCallback').on("click", function () {
        $('.popup, .overlay-feedback').fadeIn(600);
    });

    $('.jsClosePopup').on("click", function (e) {
        $('.overlay').fadeOut("slow");
    });


    $('.main-slider').slick({
        slidesToShow: 1,
        arrows: false,
        autoplay: false,
        autoplaySpeed: 3000,
        slidesToScroll: 1,
        infinite: true,
        dots: true,
        fade: true,
        cssEase: 'linear',
        responsive: [{
            breakpoint: 991,
            settings: {
                slidesToShow: 1,
                prevArrow: '<div class="slider-arrow_none"></div>',
                nextArrow: '<div class="slider-arrow_none"></div>',
                autoplay: false,
            }
        }
        ]
    });

    setTimeout(function () {
        // $('.slick-dots li button').append('<span>');
    }, 300);

    $('.main-news__slider').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        adaptiveHeight: true,
        autoplay: false,
        arrows: true,

        prevArrow: '<div class="main-news__slider-arrow main-news__slider-arrow-left"></div>',
        nextArrow: '<div class="main-news__slider-arrow main-news__slider-arrow-right"></div>',

        responsive: [{
            breakpoint: 1600,
            settings: {
                slidesToShow: 4,
                arrows: true,
            }
        }, {
            breakpoint: 1199,
            settings: {
                slidesToShow: 3,
            }
        }, {
            breakpoint: 991,
            settings: {
                slidesToShow: 2,
            }
        }, {
            breakpoint: 575,
            settings: {
                slidesToShow: 1,
            }
        }
        ],

    });

    // Маска ввода номера телефона
    $('.jsPhoneMask').inputmask("+7(999)999-99-99");

    $('textarea[name="message"]').on('input', function () {
        $(this).val($(this).val().replace(/^\s+/i, ''));
    });

    $('input[name="name"]').on('input', function () {
        $(this).val($(this).val().replace(/^\s+/i, ''));
    });

    $('body').on('wpcf7mailsent', function (e) {
        $('.overlay').fadeOut("slow");
        $("#popupThx").fadeIn("slow");
    });

    function showError(target, msg) {
        target.addClass('form_invalid');
        if (!target.find('.form__error').length) {
            var msgBlock = $('<div>').text(msg).addClass('form__error');
            target.append(msgBlock);
        }
        target.find('.form__error').slideDown();
    }

    function hideError(target) {
        target.removeClass('form_invalid');
        target.find('.form__error').slideUp();
    }

    function isValidEmail(string) {
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        return reg.test(string);
    }

    function isValidPhone(string) {
        var reg = /\+\d{1}\(\d{3}\)\d{3}-\d{2}-\d{2}/g;
        return reg.test(string);
    }


    $(".form__attach input").change(function () {
        var filename = $(this).val().replace(/.*\\/, "");
        $(".form__attach-text").html(filename);
    });

});